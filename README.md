## Grafamin

Configuration for Grafana to visualize data in [GarminDB](https://github.com/tcgoetz/GarminDB)

Available Dashboards:
   1. Activities
   1. Daily Summary
   1. Continuous Monitoring

### Requirements

1. Docker: The setup runs the [Grafana OSS container image](https://hub.docker.com/r/grafana/grafana-oss) with provisioned dashboards and the [SQLite Datasource Plugin](https://grafana.com/grafana/plugins/frser-sqlite-datasource/) installed.

2. GarminDB: Data from your Garmin Device or Garmin Connect should be downloaded to your machine using GarminDB. It creates SQLite databases that Grafamin queries from. By default these can be found in `$HOME/HealthData/DBs` and this path can be changed in `start.sh` if needed.

### Usage

```bash
$ bash start.sh
```

Open `localhost:3000` in your browser

### Notes

1. The Grafana instance has authentication disabled for convenience. Use it at your own risk.
2. Grafana takes a while to start up as it runs migrations and downloads the plugin.
3. A big thank you to the GarminDB project!!
4. Files:
   1. `config.ini`: Grafana instance configuration
   2. `dashboards.yml`, `datasources.yml`: Provision datasources and dashboards during start up
   3. `*.json`: Dashboard files 