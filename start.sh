#!/bin/bash

docker run -p 3000:3000 --rm \
   -e "GF_INSTALL_PLUGINS=frser-sqlite-datasource" \
   -v $PWD/config.ini:/etc/grafana/grafana.ini \
   -v $HOME/HealthData/DBs:/data \
   -v $PWD:/etc/grafana/provisioning/datasources/ \
   -v $PWD/dashboards.yml:/etc/grafana/provisioning/dashboards/dashboards.yml \
   -v $PWD:/var/lib/grafana/dashboards/grafamin/ grafana/grafana-oss
